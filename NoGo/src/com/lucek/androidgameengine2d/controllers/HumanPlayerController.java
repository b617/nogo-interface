/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucek.androidgameengine2d.controllers;

import android.graphics.Point;
import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class HumanPlayerController extends AbstractPlayerController{

    Scanner input;
    
    public HumanPlayerController(long movementTime) {
        super(movementTime);
        input = new Scanner(System.in);
    }

    
    
    @Override
    public Point MakeMove(Point lastOpponentsMove) throws NoMoveMadeException {
        System.out.println("Player "+GetColour().toString()+", please make a move");
        int x, y;
        Point returnValue = null;
        
        while(true){
            String[] temp = input.nextLine().trim().split(" ");
            try{
                x = Integer.parseInt(temp[0]);
                y = Integer.parseInt(temp[1]);
                
                returnValue = new Point(x,y);
                if(IsMoveValid(returnValue)) {
                    return returnValue;
                }
                else {
                    System.err.println("Invalid move.");
                }
                
            }catch(Exception e){
                //if anything goes wrong, try again!
                System.err.println("Input couldn't be parsed. Try again.");
            }
            
        }
    }
    
}
