package com.lucek.androidgameengine2d;

import com.lucek.androidgameengine2d.controllers.HumanPlayerController;
import com.lucek.androidgameengine2d.controllers.RandomMoveAI;
import com.lucek.androidgameengine2d.core.graphics.Window;

import com.lucek.androidgameengine2d.game.Map;
import com.lucek.androidgameengine2d.gameplay.Game;

/**
 * Created by lukas on 12.10.2016.
 */

public class Main {

    private Map map;
    private Window m_Window;

    private Game gameInstance;

    public static void main(String[] args){
		
	Main main=new Main();
	main.Create();
	
        while(true){
            try{
                main.Update(0);
            }
            catch (Game.GameIsOverException e) {
                System.out.println("Game finished. Winner: "+e.winner.toString());
                break;
            } 
        }
        
		
        
    }
	
    public Main() {
        Create();
    }

	
    public void Create(){
        map = new Map();
        m_Window = new Window();
        gameInstance = new Game(new RandomMoveAI(1000), new RandomMoveAI(5000), map, m_Window);
    }

    // DeltaTime in ms
    public void Update(float DeltaTime) throws Game.GameIsOverException {

        try {
            gameInstance.Update();
            // draw every pawns
            map.draw();
            System.out.println();
        }
        catch (Game.InvalidMoveException e){
            System.err.println(e.getMessage());
        }

    }
}
