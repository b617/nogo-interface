package com.lucek.androidgameengine2d.game;

/**
 * Created by lukas on 13.10.2016.
 */


public class Map {

    private Field[][] Fields;
	
	char Black = 'X';
	char White = 'O';
	char Empty = '-';
	char InvalidFieldArg = '?';

	
	
    public Map(){
	Fields = new Field[9][9];
        clearMap();
    }

    public Map setField(int pos[], Field field){
        int x = pos[0];
        int y = pos[1];
        this.Fields[y][x] = field;
        return this;
    }

    public Map setField(int x, int y, Field field){
        this.Fields[y][x] = field;
        return this;
    }

    public Field getField(int pos[]){
        int x = pos[0];
        int y = pos[1];
        return this.Fields[y][x];
    }

    public Field getField(int x, int y){
        return this.Fields[y][x];
    }


    public Map clearMap(){
        for(int y=0;y<Fields.length;y++) {
            for(int x=0;x<Fields[0].length;x++){
                Fields[y][x] = Field.EMPTY;
            }
        }
        return this;
    }

    private char FieldToChar(Field f){
        switch(f)
        {
            case EMPTY:
                return Empty;
            case BLACK:
                return Black;
            case WHITE:
                return White;
        }
        return InvalidFieldArg;
    }
	
    public Map draw(){
		
        for(int x=0;x<Fields.length;x++){
            for(int y=0;y<Fields[x].length;y++){
		System.out.print(FieldToChar(Fields[x][y]));
		System.out.print(' ');
            }
            
            System.out.println();
	}

        return this;
    }


}
